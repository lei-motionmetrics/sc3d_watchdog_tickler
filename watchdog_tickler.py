#!/usr/bin/env python
import sys, os, argparse, errno, json, datetime, time
import time
import datetime
import json

from sysfs.gpio import Controller
from sysfs.gpio import OUTPUT, INPUT, RISING

class WatchdogTickler:

    def __init__(self, base_logging_dir, gpio_pin, set_value, reset_value):
        self.base_logging_dir = base_logging_dir
        self.gpio_pin = gpio_pin
        self.set_value = set_value
        self.reset_value = reset_value
        self.nowdatetime = int(time.time())

    def get_dir_structure(self):
        full_dir = os.path.join(self.base_logging_dir, datetime.datetime.utcfromtimestamp(self.nowdatetime).strftime('%Y{0}%m{0}%d').format(os.path.sep))
        return full_dir

    @staticmethod
    def get_default_config():
        config = { 
            'enabled' : True,
            'gpio_pin' : 298,
            'set_value' : 1,
            'reset_value' : 0
        }
        return config
    
    @staticmethod
    def get_os_uptime():
        with open('/proc/uptime', 'r') as f:
            uptime_seconds = float(f.readline().split()[0])
        return uptime_seconds

    @staticmethod
    def get_package_version():
        current_dir = os.path.dirname(os.path.realpath(__file__))
        package_path = os.path.join(current_dir, '../')
        with open(os.path.join(package_path, "package_manifest.txt"), 'r') as f:
            content = f.read().split()

        return content
    
    def get_header(self):
        header = {
                'priority' : 1,
                "source" : "MOTIONMETRICS",
                "time" : self.nowdatetime,
                "timeoffset" : 0
        }
        return header
    
    def get_body(self, result):
        body = {
            "logversion" : "1.1",
            "subsystem" : "SC3D_WATCHDOG_TICKLER",
            "gpio_pin" : self.gpio_pin,
            "result": result,
            'os_uptime': self.get_os_uptime()
        }
        return body
    
    def run(self):
        Controller.available_pins = [self.gpio_pin]
        led_pin = Controller.alloc_pin(self.gpio_pin, OUTPUT)
        led_pin.set()
        setValue = led_pin.read()
        print("set value: " + str(setValue))

        time.sleep(0.1)

        led_pin.reset()
        resetValue = led_pin.read()
        print("reset value: " + str(resetValue))

        if setValue == self.set_value and resetValue == self.reset_value:
            result = "success"
        else:
            result = "fail: set value->" + str(setValue) + "(expected " + str(self.set_value) + "), reset value->" + str(resetValue)+"(expected " + str(self.reset_value) + ")"
        print(result)

        if self.base_logging_dir is not None:
            data = { 
                'header' : self.get_header(),
                'body' : self.get_body(result)
            }
            base_dir = self.get_dir_structure()
            os.makedirs(base_dir, exist_ok=True)
            log_name = datetime.datetime.utcfromtimestamp(self.nowdatetime).strftime('SC3D_WDTTickler_%Y%m%d_%H%M%S.gmp')
            with open(os.path.join(base_dir, log_name), 'w') as f:
                json.dump(data, f, sort_keys=True, indent=4, separators=(',', ': '))

        if result == "success":
            return 0
        else:
            return 1


def parse_wdt_config(wdt_config_filename):
    config = {
        'enabled' : True,
        'gpio_pin' : 298,
        'set_value' : 1,
        'reset_value' : 0
    }
    with open(wdt_config_filename) as file:
        data = json.load(file)
    if 'alwaysTickle' in data:
        config['enabled'] = data['alwaysTickle']
    if 'gpio_pin' in data:
        config['gpio_pin'] = data['gpio_pin']
    if 'set_value' in data:
        config['set_value'] = data['set_value']
    if 'reset_value' in data:
        config['reset_value'] = data['reset_value']
    return config


def main(arguments):

    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument('-d','--logdir', help="directory specified if writing all logs", required=True)
    parser.add_argument('--WDTconfig', help="the Watchdog tickler config file.", required=True)
    args = parser.parse_args(arguments)

    config = WatchdogTickler.get_default_config()

    if(args.WDTconfig is not None):
        print("Using WDTconfig at {}".format(args.WDTconfig))
        config = parse_wdt_config(args.WDTconfig)

    if config['enabled'] == True:
        print("Running watchdog tickler as it is enabled")
        watchdogTickler = WatchdogTickler(args.logdir, config['gpio_pin'], config['set_value'], config['reset_value'])
        return watchdogTickler.run()
    else:
        print("Not running watchdog tickler as it is not enabled")
        return 0

if __name__ == '__main__':
     sys.exit(main(sys.argv[1:]))

    