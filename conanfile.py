from conans import ConanFile
import os, unittest

class SC3D_WatchdogTicklerConan(ConanFile):
    name = "SC3D_WatchdogTickler"
    version = "1.0.12"
    url = "git@bitbucket.org:motionmetricsdev/{}.git".format(name.lower())
    build_policy = "missing"
    
    def source(self):
        self.run("git clone {}".format(self.url))
        self.run("git checkout v%s" % self.version, cwd=os.path.join(self.source_folder, self.name.lower()))
    
    def test(self):
        self.run("cd %s && python3 -m unittest discover" % self.source_folder)

    def package(self):
        self.copy("*", excludes=['.gitignore', '*__pycache__*'], dst=self.package_folder)
    
    def deploy(self):
        self.copy("*", excludes=['.gitignore', '*__pycache__*'], src=os.path.join(self.package_folder, self.name.lower()), dst=self.install_folder)
        with open( os.path.join(self.install_folder, "versionString.txt"), 'w' ) as file:
            file.write(self.name + '_' + str(self.version).replace('.', '_'))


    def package_info(self):
        self.env_info.PYTHONPATH.append(self.package_folder) 